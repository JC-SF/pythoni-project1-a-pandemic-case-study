# Python Project 1: A Pandemic Case Study

GitLab Link: https://gitlab.com/JC-SF/pythoni-project1-a-pandemic-case-study/-/tree/master

Date: March 29th

ID: 1733574

Name: Julian Mora Mesa

ID: 1533920

Name: Juan-Carlos Sreng-Flores

## Motive
This project's goal is to web scrape, sort and clean data from the current pandemic situation. It will plot graphs, and sort data accordingly to the user's wishes.

## Setup
The program expects to find a login.txt file where it stores the login information for the user in order to create a MySQL Connection.
Expected file location: pythonProject1/files/login.txt
The file must be written in the folliwing format.
``` 
USERNAME
HOST
PASSWORD
```
The file must be created prior to running the program.
An existing TableSchemas.txt file holds information about the table design, and information for its creation.

File location: `pythonProject1/database/TableShemas.txt`

Some html files were saved in order to retrieve local information about the covid cases.

Folder: `pythonProject1/local_html/`

A country json file were given to also include information about countries, their neighbours, and their distance.

File location: `pythonProject1/countries_json/country_neighbour_dist_file.json`

## Program Files
```
main.py
CoronaDataManage.py
WebScraper.py
DatabaseAPI.py
CountriesDataJSON.py
ExploreData.py
```
The main.py program handles the shell, and user interface. 

CoronaDataManage.py will handle merging all the records and inserting it into the database using the DatabaseAPI module, and will gather all the records parrsed from CountriesDataJSON.py and WebScraper.py. Once all the records are inserted into the database, it will also prompt the plot to be shown to the user. It also handles the creation of the database if the database has not been created before.

WebScraper.py will use the BeautifulSoup module from bs4 in order to scrape the website either from the internet, or the local_html files. 

DatabaseAPI.py is a simple API to allow database processes in a much user friendly way. To create an instance of a connection, it requries the login.txt path in order to gather the necessary information for the connection.

CountriesDataJSON.py will fetch for the countries information in the country_neighbour_dist_file.json and convert it into records to be inserted in the database.

Finally, ExploreData.py will query data from the database, and use it to plot them from a dataframe.
## Run
To run the program, you may need to first write the login information in the login.txt file. Then run the main.py file.
The remaining necessary files should be already properly formatted for program execution.

