# Python Project 1: A Pandemic Case Study
# Date: March 29th
# ID: 1733574
# NAME: Julian Mora Mesa
# ID: 1533920
# NAME: Juan-Carlos Sreng-Flores
from DatabaseAPI import DBConn
from CountriesDataJSON import CountriesDataJSON
from WebScraper import WebScraper
from ExploreData import ExploreData

# This class manages the overall desired creation of the database and tables,
# the parsing of the json_countries file, the parsing of the scraped data from 
# file and/or from the web, and the insertion of records in the tables.
# It shall be used by the main method to execute in order to set up the database.
class CoronaDataManage:
    # constructor method. Creates attributes, and save them for future use with 
    # database creation.
    def __init__(self, login_file, dbname, schema_file, countries_json_file, url, local_html_dir):
        self.__login_file = login_file
        self.__dbname = dbname
        self.__schema_file = schema_file
        self.__countries_json_file = countries_json_file
        self.__webscraper = WebScraper(url)
        self.__local_html_dir = local_html_dir
        self.__dbconn = DBConn(self.__login_file)
        self.__parser = CountriesDataJSON()
        self.__data_plot = ExploreData(self.__dbconn)
        

    # this method will create the database, and store the data accordingly from
    # the files parsed. It will set up the tables, and also populate them.
    def create_corona_db_and_tables(self):
        # create connection from filename path.
        # dbconn = DBConn(self.__login_file)
        self.__dbconn.create_db(self.__dbname)
        self.__dbconn.select_db(self.__dbname)
        # read table schemas from file
        table_schemas = DBConn.read_schemas_from_file(self.__schema_file)
        # create tables from table schema
        self.__dbconn.create_tables(table_schemas)
        # commit
        self.__dbconn.commit()
        
        
    def check_db_exist(self):
        databases = self.__dbconn.show_db()
        dbexist = False
        for rec_tuple in databases:
            for db in rec_tuple:
                if db.lower() == self.__dbname.lower():
                    dbexist = True
        return dbexist
    
    def scrape_to_local(self):
        self.__webscraper.request_html()
        self.__webscraper.create_local_file(self.__local_html_dir)
        
    
    def scrape_local_html(self, user_date, country):
        if self.__webscraper.local_files_exist(user_date):
            # returns a list of tuples with the name of the tables.
            self.__dbconn.select_db(self.__dbname)
            table_names = self.__dbconn.show_tables()
            # parse countries from file
            self.__parser.parse_json_countries_and_neighbours_to_records(self.__countries_json_file)
    
            # gather table names, and their records.
            table_name_schema_records = dict()
            for table_name in table_names:
                    table_name_schema_records[table_name[0]] = {"mysql_schema": self.__dbconn.desc_table(table_name[0])}
    
            table_records = []
            # make sure all country_names exist from both json and html files.
            self.__webscraper.extract_data_rows(user_date)
            html_tuple_list = self.__webscraper.return_tuple_list()
            country_names = set()
            for tuple_row in html_tuple_list:
                country_names.add(tuple_row[2]) #name of country should be at index 2
            self.__parser.add_country_names(country_names)
            
            # add them all in a dictionary of records
            table_records.append(self.__parser.get_country_names_records())
            table_records.append(self.__parser.get_country_borders_records())
            table_records.append(html_tuple_list)
            # print(table_name_schema_records)
            
            #prepare values for insertion
            for country_value, records in zip(table_name_schema_records.values(), table_records):
                country_value['records'] = records
            #print(table_records)
            for k, v in table_name_schema_records.items():
                if k=='country':
                    self.__dbconn.populate_table_ignore_dup(k, v['mysql_schema'], v['records'])
                else:
                    self.__dbconn.populate_table_replace_old(k, v['mysql_schema'], v['records'])
            # commit
            self.__dbconn.commit()
            
            self.__data_plot.plot_all_graphs(user_date, country.upper()) #make the country name uppercase
    
    
    
    # Closes all potential connections the object will have.
    def close_connections(self):
        self.__dbconn.close_connection()

    # This parses the dictionnary rows into tuples and returns them
    def parse_dictionary_rows(self, table_dictionary):
        for row_list in table_dictionary.values():
            tuple_list = [tuple(row.values()) for row in row_list]
        return tuple_list
