# Python Project 1: A Pandemic Case Study
# Date: March 29th
# ID: 1733574
# NAME: Julian Mora Mesa
# ID: 1533920
# NAME: Juan-Carlos Sreng-Flores
import json
# This class is used to parse data whether it is from the json file or from a 
# local html file, and keeps the set of the records as to not duplicate the insertion.
class CountriesDataJSON:
    def __init__(self):
        self.__country_list_records = None
        self.__country_list_json = None
        self.__country_set = None
        self.__country_names_records = None
        self.__country_neighbours_set = None
        self.__country_neighbours_records = None
        
    # this method will clean and parse the data from the json file
    # expecting the country_neighbour_dist_file.py to read from.
    # The method will read, and parse all data within it, and it will
    # keep a copy of the records for further record processing. The 
    # records are stored in a set so that no duplication happens.
    def parse_json_countries_and_neighbours_to_records(self, json_file):
        file_obj = open(json_file)
        self.__country_list_json = json.loads(file_obj.read())
        self.__country_neighbours_set= set()
        self.__country_set = set()
        for country_json in self.__country_list_json:
            for country in country_json:
                uppercase_country = country.upper()
                for country_neighbour in country_json[country]:
                    uppercase_neighbour = country_neighbour.upper()
                    self.__country_neighbours_set.add((uppercase_country, uppercase_neighbour, country_json[country][country_neighbour]))
                    self.__country_set.add((uppercase_neighbour,))
                self.__country_set.add((uppercase_country,))
        
        file_obj.close()
    
    # this method retrieves the values of the records (stored in set)
    # and returns a list containing the records in tuples. It specifically
    # returns the records processed for the country table in MySQL
    # returns a set of all the data.
    def get_country_names_records(self):
        self.__country_names_records = list(self.__country_set)
        return self.__country_names_records
    
    # this method retrieves the values of the records (stored in set)
    # and returns a list containing the records in tuples. It specifically
    # returns the records processed for the country table in MySQL
    def get_country_borders_records(self):
        self.__country_neighbours_records = list(self.__country_neighbours_set)
        return self.__country_neighbours_records
    
    def add_country_names(self, country_list):
        country_set = []
        for country_name in country_list:
            country_set.append((country_name,))
        self.__country_set = self.__country_set.union(country_set)