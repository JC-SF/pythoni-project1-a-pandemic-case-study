# Python Project 1: A Pandemic Case Study
# Date: March 29th
# ID: 1733574
# NAME: Julian Mora Mesa
# ID: 1533920
# NAME: Juan-Carlos Sreng-Flores
import pandas as pd
import matplotlib.pyplot as plt
from datetime import date, timedelta


# This class will plot the data from the DBConn Object, and will plot the returned frames
class ExploreData:
    # Constructor method
    def __init__(self, dbconn):
        self.__dbconn = dbconn

    # This method plots the three plot graph
    def __three_key_plot(self,user_date, country):
        startdate = user_date-timedelta(days=2)
        enddate = user_date+timedelta(days=3)
        # Query the DB and create the 3 key Dataframe for the specified country
        query = f"select `Date`, `NewCases`, `NewDeaths`, `NewRecovered` from country_corona_date\
        where `Country Name` = '{country}'\
        and (`Date` BETWEEN '{startdate}' and '{enddate}');"
        three_key_dataframe = self.__dbconn.return_dataframe_query(query)
        # Plotting of graph
        three_key_dataframe.plot.bar(x='Date', color=['blue', 'green', 'red'],
                                     title=f'6-days key indicators evolution {country}')
        plt.ylabel("3-main Indicators")

    # This method plots the bar graph comparing the specified country and its neighbour with the longest border
    def __plot_border_newCases(self, user_date, country):
        #calculate the start date and end date for 6 days worth of data
        startdate = user_date-timedelta(days=2)
        enddate = user_date+timedelta(days=3)
        # Query the DB and store values in their own Dataframes(df)
        query_country_df = f"select `Date`, `NewCases` from country_corona_date\
        where `Country Name` = '{country}'\
        and (`Date` BETWEEN '{user_date-timedelta(days=2)}' and '{user_date+timedelta(days=3)}');"
        country_dataframe = self.__dbconn.return_dataframe_query(query_country_df)
        query_column_neighbour = f"select `Date`,`Country Name`, `NewCases` from country_corona_date \
			where `Country Name` = \
            (select `Neighbour Country` from country_borders\
            where Distance = \
            (select MAX(Distance) from\
            country_borders where `Country Name` = '{country}') \
            AND `Country Name` = '{country}')\
            AND (`Date` BETWEEN '{startdate}' and '{enddate}');"
        three_column_neighbour_dataframe = self.__dbconn.return_dataframe_query(query_column_neighbour)
        # Creating neighbour dataframe without Country Name, to be then merged with country_dataframe
        neighbour_dataframe = pd.DataFrame(three_column_neighbour_dataframe, columns=['Date', 'NewCases'])
        merged_df = pd.merge(country_dataframe, neighbour_dataframe, on='Date')
        neighbour_country_name = three_column_neighbour_dataframe['Country Name'][0]
        # Creation of the aggregate plot
        merged_df.plot.bar(x='Date', color=['r', 'b'], title=f'6-days Newcases comparison-{country} with '
                                                                        f'neighbour {neighbour_country_name}')
        plt.ylabel("New Cases")
        plt.legend([f'NewCases-{country}', f'NewCases-{neighbour_country_name}'])

    # This method plots the comparison of Deaths/1M pop of the specified country with its (max) 2 neighbours
    def __plot_Deaths_1m(self, user_date, country):
        # Query the DB and store values in their own Dataframes(df). Their data will be extrapolated into an empty
        # Dataframe
        startdate = user_date-timedelta(days=2)
        enddate = user_date+timedelta(days=3)
        empty_dataframe = pd.DataFrame()
        query_country_neighbour_deaths = f"select `Date`,`Country Name`,`Deaths/1M pop` from country_corona_date\
        where `Country Name` IN \
		(SELECT * FROM (SELECT `Neighbour Country` from country_borders \
			where `Country Name` = '{country}'\
			ORDER BY Distance DESC LIMIT 2) as t1)\
        AND (`Date` BETWEEN '{startdate}' AND '{enddate}')\
        UNION\
        select `Date`,`Country Name`,`Deaths/1M pop` from country_corona_date \
	where `Country Name` = '{country}'\
    AND (`Date` BETWEEN '{startdate}' AND '{enddate}');"
        deaths_dataframe = self.__dbconn.return_dataframe_query(query_country_neighbour_deaths)
        # Adding the Date Column in the empty dataframe, to be merged with other data later.
        restriction = deaths_dataframe['Date'] >= startdate
        restriction2 = deaths_dataframe[restriction]['Date'] <= enddate
        empty_dataframe['Date'] = deaths_dataframe[restriction2]['Date'].drop_duplicates().tolist()
        # Parsing country names
        country_names = (deaths_dataframe['Country Name'].drop_duplicates().values.tolist())
        
        # Adding column values for respective countries
        for country_name in country_names:
            restriction = (deaths_dataframe['Country Name'] == country_name)
            empty_dataframe[country_name] = deaths_dataframe[restriction]['Deaths/1M pop'].tolist()

        # Plotting newly created dataframe
        empty_dataframe.plot.bar(x='Date', color=['r', 'b', 'g'],
                                 title=f'6-days Deaths/1M pop comparison-{country} with 2 neighbours')
        plt.ylabel("Deaths/1M pop")

        # Reformatting country_names list to fit legend format
        country_names.insert(0, country)
        position = 0
        for country_name in country_names:
            country_names[position] = "Deaths/1M pop-" + country_name
            position += 1

        plt.legend(country_names)

    # This method will be called once in main and will run all the helper methods to output the three graphs
    def plot_all_graphs(self, user_date, country):
        # Call each plot and convert user_date str into a valid date.
        today = date.today()
        user_date = date(today.year, today.month, int(user_date))
        self.__three_key_plot(user_date,country)
        self.__plot_border_newCases(user_date, country)
        self.__plot_Deaths_1m(user_date, country)
        plt.show()
