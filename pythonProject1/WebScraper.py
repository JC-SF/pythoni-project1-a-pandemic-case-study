# Python Project 1: A Pandemic Case Study
# Date: March 29th
# ID: 1733574
# NAME: Julian Mora Mesa
# ID: 1533920
# NAME: Juan-Carlos Sreng-Flores
from bs4 import BeautifulSoup
from datetime import datetime
from urllib.request import urlopen, Request
import re


#   This class is for scraping a webpage and saving it locally as
#   an html file.
class WebScraper:

    # Initialize the WebScrapper object and its properties
    def __init__(self, url, date=f'{datetime.date(datetime.now())}'):
        # self.__page = None
        self.__date = date
        self.__soup = None
        self.__url = url
        self.indented_page = None
        self.table_dict = {'today': [],
                           'yesterday': [],
                           'yesterday2': [],
                           'tomorrow': [],
                           'tomorrow2': [],
                           'tomorrow3': []}

    # requests the html data from the server and creates a BeautifulSoup object
    # in order to properly store the page(proper indentation included).
    def request_html(self):
        request_obj = Request(self.__url, headers={'user-Agent': 'Mozilla/5.0'})
        html_obj = urlopen(request_obj)
        self.__soup = BeautifulSoup(html_obj, 'html.parser')
        self.indented_page = self.__soup.prettify()
        html_obj.close()

    # prints the html page
    # **FOR DEBUGGING**
    def print_html(self):
        print(self.indented_page)

    # returns the html page
    # **FOR DEBUGGING**
    def return_html_page(self):
        return self.indented_page

    # Creates/labels the local copy of the scraped html page
    def create_local_file(self, local_html_dir):
        path = f'{local_html_dir}\\local_page{self.__date}.html'
        result_obj = open(f'{local_html_dir}\\local_page{self.__date}.html', 'wt', encoding="utf-8")
        result_obj.write(self.indented_page)
        print(f'Successfully saved website in following directory: {path}')
        result_obj.close()

    def local_files_exist(self, user_date):
        exist = False
        year_month = datetime.date(datetime.now())
        try:
            path = f'local_html\\local_page{year_month.strftime("%Y-%m-")}{user_date}.html'
            local_page1 = open(path, 'r',
                               encoding="utf-8")
            path = f'local_html\\local_page{year_month.strftime("%Y-%m-")}{int(user_date) + 3}.html'
            local_page2 = open(path, 'r',
                               encoding="utf-8")
            local_page1.close()
            local_page2.close()
            exist = True
        except FileNotFoundError:
            print(f'Could not find one of the two required local files: {path}')
        return exist

    # This method extracts the 6 tables from the local html pages and adds them to the global variable
    def __extract_tables(self, user_date):
        year_month = datetime.date(datetime.now())
        local_page1 = open(f'local_html\\local_page{year_month.strftime("%Y-%m-")}{user_date}.html', 'r',
                           encoding="utf-8")
        local_page2 = open(f'local_html\\local_page{year_month.strftime("%Y-%m-")}{int(user_date) + 3}.html', 'r',
                           encoding="utf-8")
        bs1 = BeautifulSoup(local_page1, "html.parser")
        bs2 = BeautifulSoup(local_page2, "html.parser")
        local_page1.close()
        local_page2.close()
        table_list = []
        table_today = bs1.find('table', {"id": "main_table_countries_today"})
        table_yesterday = bs1.find('table', {"id": "main_table_countries_yesterday"})
        table_yesterday2 = bs1.find('table', {"id": "main_table_countries_yesterday2"})
        table_tomorrow = bs2.find('table', {"id": "main_table_countries_today"})
        table_tomorrow2 = bs2.find('table', {"id": "main_table_countries_yesterday"})
        table_tomorrow3 = bs2.find('table', {"id": "main_table_countries_yesterday2"})
        table_list.append(table_yesterday2)
        table_list.append(table_yesterday)
        table_list.append(table_today)
        table_list.append(table_tomorrow3)
        table_list.append(table_tomorrow2)
        table_list.append(table_tomorrow)

        return table_list

    # This method extracts the data from the 3 tables and adds them to the global tables_dictionary
    def extract_data_rows(self, user_date):
        # Initializing the tables from local_pages, the list of all the tables respective dates
        # and the list of column names for the table rows.
        corona_table_list = self.__extract_tables(user_date)
        dates_list = self.__create_dates_list(user_date)
        column_names = self.__create_column_list(corona_table_list)
        table_date_position = 0  # used to keep track of the current tables date.

        # using both the table and the dictionary key as iterators (6 total loops),
        # for each individual table, extract all the rows and its corresponding data.
        for table, key in zip(corona_table_list, self.table_dict.keys()):
            table_data = []
            # extract rows in current table and add to list and subsequently dictionary
            for current_row in table.tbody("tr"):
                # used dict for easy labeling/readability + easily converted to tuple for DB insertion
                trow = {'Date': dates_list[table_date_position]}
                is_rank_null = False
                for td, column in zip(current_row('td'), column_names):
                    # The reason why i would get the continents/missing data is because some ranks were empty.
                    # For now i just check if the row is an empty rank, if so, is_rank_null = True, only add to
                    # table_data if (is_rank_null == False)
                    if column == '#' and td.text.strip() == '':
                        is_rank_null = True
                    if not is_rank_null:
                        trow[column] = td.text.strip()
                    else:
                        break
                if not is_rank_null:
                    table_data.append(trow)
            table_date_position += 1
            # clean/prepare row values for DB insertion
            # add missing fields for table.
            self.table_dict[key] = self.__clean_data(table_data)

    # This method will save the column names for the table rows.
    def __create_column_list(self, corona_table_list):
        column_names = []
        for column in corona_table_list[0].find_all('th'):
            cleaner_column = column.text.replace('\n', '').replace(u'\xa0', u' ').strip()
            column_names.append(re.sub("\\s+", ' ', cleaner_column))
        return column_names

    # This method will take the users requested date, and expand it to cover 6 days worth of table data.
    # EX: user_date = 16; list will comprise dates YEAR-MON-14 through Year-MON-19.
    def __create_dates_list(self, user_date):
        dates_list = []
        year_month = datetime.date(datetime.now())
        for i in range(0, 6):
            dates_list.append(f'{year_month.strftime("%Y-%m-")}{(int(user_date) - 2) + i}')
        return dates_list

    # prints the global dict holding the rows for each table
    # for debugging
    def print_dict(self):
        for table in self.table_dict.keys():
            for table_row in self.table_dict[table]:
                print(table_row)
            print("--" * 100)

    # This method takes the tables rows and cleans the values inside, making sure that they are appropriate
    # for the subsequent database insertion.
    def __clean_data(self, row_list):
        for row in row_list:
            for column in row.keys():
                # remove decimal separators from column values
                clean_column_value = row[column].replace(',', '').replace('+', '').strip()
                if clean_column_value.isdigit():
                    row[column] = int(clean_column_value)
                # replacing empty data values with 0
                if clean_column_value == '' or clean_column_value == 'N/A':
                    row[column] = None
        return row_list

    # This method converts the data from dictionary records to tuple records
    # for database insertion.
    def return_tuple_list(self):
        tuple_list = []
        for table in self.table_dict.keys():
            for rows in self.table_dict[table]:
                tuple_row = [value.upper() if type(value) == str else value for value in rows.values()]
                tuple_list.append(tuple_row)
        return tuple_list

    # returns the table_dict
    def get_table_dict(self):
        return self.table_dict
