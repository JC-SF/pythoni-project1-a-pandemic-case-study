# Python Project 1: A Pandemic Case Study
# Date: March 29th
# ID: 1733574
# NAME: Julian Mora Mesa
# ID: 1533920
# NAME: Juan-Carlos Sreng-Flores
import mysql.connector as myc
import pandas as pd


# This Class is used to create and manipulate databases in mysql. It uses a file
# to login into the mysql server. 
class DBConn:
    # Class attributes
    __USER = 0
    __HOST = 1
    __PASSWORD = 2

    # The constructor will use a file to read from, and log in into a MySQL server.
    def __init__(self, filename):
        file_obj = open(filename, 'r', encoding='UTF-8')
        # Take user info for connection
        login = file_obj.readlines()
        # remove '\n' from user login
        for i in range(0, len(login)):
            login[i] = login[i].replace('\n', '')
        # check integrity of login
        if len(login) == 3:
            self.__db_obj = myc.connect(host=login[DBConn.__HOST], user=login[DBConn.__USER],
                                        password=login[DBConn.__PASSWORD])
            self.__my_cursor = self.__db_obj.cursor()
            print('Successfully logged in.')
        else:
            print(
                f'Check login file `{filename}`. It should only have 3 lines, in order as follows: \nUSERNAME,\nHOST,\nPASSWORD')

    # this function creates the DB and displays any possible errors
    def create_db(self, dbname):
        try:
            self.__my_cursor.execute(f'drop database if exists {dbname}')
            self.__my_cursor.execute(f'CREATE DATABASE {dbname}')
            print(f'Database `{dbname}` successfully created.')
        except myc.Error as e:
            print(f'Error creating `{dbname}` from MYSQL: {e}')

    # This creates the tables from a table schema dictionary. It will essentially
    # run through a whole dictionary of table names as keys, and table schemas (str)
    # as values, and create them accordingly.
    def create_tables(self, table_schemas):
        try:
            for table_name, table_schema in table_schemas.items():
                self.__my_cursor.execute(f'DROP TABLE IF EXISTS {table_name}')
                self.__my_cursor.execute(f'CREATE TABLE {table_name} ({table_schema})')
            print('Table(s) successfully created.')
        except myc.Error as e:
            print('Error creating table from MYSQL: {}'.format(e))

    # This method will return a list of all the databases that can
    # can be seen from the current connection.    
    def show_db(self):
        returnValue = None
        try:
            query = 'show databases'
            self.__my_cursor.execute(query)
            returnValue = self.__my_cursor.fetchall()
        except myc.Error as err:
            print(f'Could not show tables: {err}')
        return returnValue

    # This function enters an existing DB, if no input is provided then the DB defaults to none and allows the user
    # to choose the database they want to join
    def select_db(self, DB_name=None):
        try:
            if DB_name is None:
                self.__my_cursor.execute('SHOW DATABASES')
                databases = self.__my_cursor.fetchall()
                for i in databases:
                    print(i)
                db_name = input('Enter the database you wish to enter')
                self.__my_cursor.execute(f'USE {db_name}')
                print(f'{db_name} selected.')
            else:
                self.__my_cursor.execute(f'USE {DB_name}')
        except myc.Error as e:
            print('Could not select database: {}'.format(e))

    # this method drops a database
    def drop_db(self, dbname):
        try:
            self.__my_cursor.execute(f'drop database if exists {dbname}')
        except myc.Error as err:
            # print('Could not drop the database.')
            print(f'Could not drop `{dbname}` database: {err}')

    # This method returns the description of an existing database.
    def desc_table(self, table_name):
        returnValue = None
        try:
            query = 'desc {0}'.format(table_name)
            self.__my_cursor.execute(query)
            returnValue = self.__my_cursor.fetchall()
        except myc.Error as err:
            print(f'Could not describe table {err}')
        return returnValue

        # This method drops an existing table in the database.

    def drop_table(self, table_name):
        try:
            query = 'drop table {0}'.format(table_name)
            self.__my_cursor.execute(query)
        except myc.Error as err:
            print(f'Could not drop table: {err}')

    # populates an existing table in the database. It takes as input 
    # the table schema (output provided by desc_table(table_name) function), and
    # a list of tuples containing the records.
    def populate_table_replace_old(self, table_name, mysql_table_schema, list_tuples):
        returnValue = False
        columns_str = []
        for schema in mysql_table_schema: columns_str.append(f'`{schema[0]}`')
        try:
            # creates a list using the length of the first tuple.
            values = ','.join(['%s'] * (len(columns_str)))
            query = 'replace into {0} ({1}) values ({2})'.format(table_name, ','.join(columns_str), values)
            self.__my_cursor.executemany(query, list_tuples)
            print(f'Successfully populated table `{table_name}`')
            returnValue = True
        except myc.Error as err:
            print(f'Could not populate tables {err}')
        return returnValue

    def populate_table_ignore_dup(self, table_name, mysql_table_schema, list_tuples):
        returnValue = False
        columns_str = []
        for schema in mysql_table_schema: columns_str.append(f'`{schema[0]}`')
        try:
            # creates a list using the length of the first tuple.
            values = ','.join(['%s'] * (len(columns_str)))
            query = 'insert ignore into {0} ({1}) values ({2})'.format(table_name, ','.join(columns_str), values)
            self.__my_cursor.executemany(query, list_tuples)
            print(f'Successfully populated table `{table_name}`')
            returnValue = True
        except myc.Error as err:
            print(f'Could not populate tables {err}')
        return returnValue

    # returns a list of the existing tables
    # following returns a list of tuples with table names in them
    def show_tables(self):
        returnValue = None
        try:
            query = 'show tables'
            self.__my_cursor.execute(query)
            returnValue = self.__my_cursor.fetchall()
        except myc.Error as err:
            print(f'Could not show tables: {err}')
        return returnValue

    # method commit the changes in made the database.
    def commit(self):
        try:
            self.__db_obj.commit()
            print('Successfuly committed changes.')
        except myc.Error as err:
            print(f'Could not commit {err}')

    # this method will read a file and extract all of the table schemas in the file.
    # it will return a dictionary with the table names as the keys, and the table schemas
    # as values.
    def read_schemas_from_file(filename):
        fileobj = open(filename, 'rt')
        tables = fileobj.read().split('\n\n')
        table_schemas = dict()
        for table in tables:
            tablename_and_schema = table.split('\n', 1)
            table_schemas[tablename_and_schema[0]] = tablename_and_schema[1]
        fileobj.close()
        return table_schemas

    # Close connection to the DB
    def close_connection(self):
        self.__my_cursor.close()
        self.__db_obj.close()
        print('Connection to DB successfully closed')

    # This method chooses the DB and uses a pandas built in function to return a Dataframe using the query supplied.
    def return_dataframe_query(self, query):
        self.select_db("covid_corona_db_JUAN_JULI")
        return pd.read_sql_query(query, self.__db_obj)
