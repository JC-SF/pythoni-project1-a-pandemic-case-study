# Python Project 1: A Pandemic Case Study
# Date: March 29th
# ID: 1733574
# NAME: Julian Mora Mesa
# ID: 1533920
# NAME: Juan-Carlos Sreng-Flores
from CoronaDataManage import CoronaDataManage

DBNAME = "covid_corona_db_JUAN_JULI"
SCHEMA_FILE = 'database\\TableSchemas.txt'
COUNTRIES_JSON = 'countries_json\\country_neighbour_dist_file.json'
LOGIN = "files\\login.txt"
LOCAL_HTML_DIR = "local_html"
URL = "https://www.worldometers.info/coronavirus"

corona_manage = CoronaDataManage(LOGIN, DBNAME, SCHEMA_FILE, COUNTRIES_JSON, URL, LOCAL_HTML_DIR)


def main_program():
    isdbcreated = False
    if not corona_manage.check_db_exist():
        isValid = False
        while (not isValid):
            yesNo = input(f'The database `{DBNAME}` has not yet been created. Would you like to create it (y/n)? ')
            yesNo.lower()
            if yesNo[0] == 'y':
                corona_manage.create_corona_db_and_tables()
                isValid = True
                isdbcreated = True
            elif yesNo[0] == 'n':
                isValid = True
            else:
                print('Your answer is invalid.')
    else:
        isdbcreated = True
    isOver = False
    while not isOver and isdbcreated:
        print('1. Save data from website into local file. ')
        print('2. Scrape from a saved local file. ')
        print('3. Quit')
        user_choice = input('Enter your choice: ')
        if user_choice == '1':
            print()
            corona_manage.scrape_to_local()
            print()
        elif user_choice == '2':
            isValid = False
            while not isValid:
                user_date = input('Enter the date (1-31): ')
                if not user_date.isdigit():
                    print('The date entered is not a number.')
                else:
                    isValid = True
            country = input('Enter the country: ')
            print()
            corona_manage.scrape_local_html(user_date, country)
            print()
        elif user_choice == '3':
            print('\nGood bye!')
            isOver = True
        else:
            print(f'Your choice `{user_choice}` is invalid. Please try again.')


try:
    main_program()
finally:
    corona_manage.close_connections()
